#!/usr/bin/env bash

# adds command createscratch and usescratch to local commands in ~/.local/bin
# creates ~/.scratches folder for executing user
# creates ~/.scratches/commands to store createscratch and usescratch
# creates ~/.scratches/projects/
# creates ~/.scratches/projects/default project
# creates ~/.scratches/from_scratch and copy ./from_scratch there
# chmod a+x createscratch usescratch
# ...
# use from command line like
# $ ./install.sh -[i/u] -[v]
#   -v - verbose (set x)
#   -i - install
#   -u - uninstall

# break on any command's error
set -e

#------------------------ recognize options

if [[ $1 == "" ]]; then
  echo "You have to specify an option like $ ./install.sh -[i/u] -[v]"
  exit 1
fi

#------------------------ setup dir paths

home="/home/$(whoami)"

#------------------------ install

if [[ $1 == "-i" ]]; then
  echo "Installing MultiScratch"

  if [[ $2 == "-v" ]]; then
    echo "Verbose mode set"
    set -x
  fi

  mkdir $home/.fromscratch
  mkdir $home/.fromscratch/last_links
  echo "" > mkdir $home/.fromscratch/current_project
  mkdir $home/.scratches
  mkdir $home/.scratches/projects/
  cp -r $(pwd)/from_scratch $home/.scratches/from_scratch
  cp -r $(pwd)/commands $home/.scratches/commands
  chmod a+x $home/.scratches/commands/createscratch
  chmod a+x $home/.scratches/commands/usescratch
  ln -s $home/.scratches/commands/createscratch "$home/.local/bin/createscratch"
  ln -s $home/.scratches/commands/usescratch "$home/.local/bin/usescratch"

  createscratch -c default

  if [[ $2 == "-v" ]]; then
    set +x
  fi

  echo "Done ;)"
  echo "Use like \$ createscratch [ProjectName]"
  exit 1
fi

#------------------------ uninstall

if [[ $1 == "-u" ]]; then
  echo "Uninstalling MultiScratch"

  if [[ $2 == "-v" ]]; then
    echo "Verbose mode set"
    set -x
  fi

  rm -rf $home/.fromscratch
  rm -rf $home/.scratches
  rm "$home/.local/bin/createscratch"
  rm "$home/.local/bin/usescratch"

  # maybe find and remove .desktops also in the future

  if [[ $2 == "-v" ]]; then
    set +x
  fi

  echo "Done ;)"
  exit 1
fi
